from django.shortcuts import render
import os
# Create your views here.
from django.http import HttpResponse
import pandas as pd
import csv
import os
import time
from datetime import datetime
import selenium

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.action_chains import ActionChains
import os, tempfile, zipfile
from wsgiref.util import FileWrapper
from django.conf import settings
import mimetypes
from io import StringIO



def user_login(request):


    return render(request, "show_data.html",)


def data_scrap(request):
    
    driver = webdriver.Chrome("chromedriver.exe")
    driver.get("http://offstagejobs.com/jobs.php")

    print("Starting...")

    try:
        driver.find_element_by_xpath("//select[@name='page']/option[text()='Display All']").click()
        time.sleep(1)
    except NoSuchElementException:
        print('Something is not right.')

    try:
        search_button = driver.find_element_by_xpath(
            '//input[contains(@name, "query_check")]')
        ActionChains(driver).move_to_element(search_button).click().perform()
        time.sleep(15)
    except NoSuchElementException:
        print('Something is not right.')

    print("Scraping...")

    final_data = []
    html = driver.page_source
    parsed_html = BeautifulSoup(html, features="html.parser")

    data1 = parsed_html.find_all('div', attrs={'class': 'itemh1'})
    data2 = parsed_html.find_all('div', attrs={'class': 'itemh2'})
    data3 = parsed_html.find_all('div', attrs={'class': 'itemContact'})
    data4 = parsed_html.find_all('span', attrs={'class': 'emu'})
    # print("data1data1data1data1data1data1data1",data1)
    # print("data1data1data1data1data1data1data222222222",data2)
    # print("data1data1data1data1data1data1data3333333",data3)
    # print("data1data1data1data1data1data1data44444444",data4)

    i = 0
    for j in data2:
        dataitem = list()
        temp = list()
        # print(i)

        try:
            temp = data1[i + 1].text.splitlines()
            dataitem.append(temp[1])
            # print(temp[1])
        except IndexError:
            dataitem.append("")

        try:
            temp = data2[i].text.splitlines()
            dataitem.append(temp[1].strip())
            # print(temp[1].strip())
        except IndexError:
            dataitem.append("")

        try:
            temp = data3[i].text.splitlines()
            dataitem.append(temp[2])
            # print(temp[2])
        except IndexError:
            dataitem.append("")

        try:
            temp = data4[i].text.splitlines()
            dataitem.append(temp[0])
            # print(temp[0])
        except IndexError:
            dataitem.append("")
        # print("dataitemdataitemdataitemdataitemdataitemdataitemdataitem",dataitem)
        # print("final_datafinal_datafinal_datafinal_datafinal_data",final_data)
        final_data.append(dataitem)
        i = i + 1

    final_data.insert(0, ['Job Title', 'Company', 'Name', 'Email'])
    # print("final_datafinal_datafinal_datafinal_datafinal_data", final_data)
    now = datetime.now()
    name = now.strftime("%m%d%Y%H%M%S")
    fileName = name + ".csv"

    with open('data.csv', 'w+', newline='', encoding='utf-8') as fp:
        a = csv.writer(fp, delimiter=',')
        a.writerows(final_data)
    df = pd.read_csv('data.csv', delimiter=',', encoding='unicode_escape')
    # print(df)
    d1 = []
    d2 = []
    d3 = []
    d4 = []
    for i in df['Job Title']:
        d1.append(i)
    for j in df['Company']:
        d2.append(j)
    for k in df['Name']:
        d3.append(k)
    for l in df['Email']:
        d4.append(l)
    mylist = zip(d3, d1, d4, d2)
    print("done")
    # return HttpResponse(DataFrame.to_html())
    return render(request, 'show_data.html', {'name': mylist})






def send_file(request):
  
    filename     = "data.csv" # Select your file here.
    download_name ="data.csv"
    wrapper      = FileWrapper(open(filename))
    response = HttpResponse(content_type='text/csv')
    # response     = HttpResponse(wrapper,content_type=content_type)
    filename = "data.csv".format("data")
    fp = StringIO()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
    writer = csv.writer(response)
            
    with open('data.csv','rt',newline='\n', encoding='utf-8')as f:
        data = csv.reader(f)
        for row in data:
            writer.writerow(row)
    
    return response




